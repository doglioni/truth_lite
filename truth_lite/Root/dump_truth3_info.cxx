#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <truth_lite/dump_truth3_info.h>
#include <truth_lite/EventInit.h>
#include <truth_lite/CommonData.h>
#include <truth_lite/Util.h>

using namespace truth_lite;

// this is needed to distribute the algorithm to the workers
ClassImp(dump_truth3_info)

dump_truth3_info::dump_truth3_info()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}

EL::StatusCode dump_truth3_info::setupJob(EL::Job& /*job*/)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::histInitialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::fileExecute()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::changeInput(bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::initialize()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  EventInit *eventInit = dynamic_cast<EventInit*>(wk()->getAlg("EventInit"));
  if (!eventInit) {
    Error("initialize()", "Failed to find EventInit algorithm");
    return EL::StatusCode::FAILURE;
  }
  commonData = eventInit->getCommonData();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent *event = wk()->xaodEvent();
  
  auto my_truth_met = Retrieve<xAOD::MissingETContainer>(event, "MET_Truth");
  auto my_truth_jets = Retrieve<xAOD::JetContainer>(event, "AntiKt4TruthJets");
  auto my_truth_largejets = Retrieve<xAOD::JetContainer>(event, "TrimmedAntiKt10TruthJets");
  auto my_truth_electrons = Retrieve<xAOD::TruthParticleContainer>(event, "TruthElectrons");
  auto my_truth_muons = Retrieve<xAOD::TruthParticleContainer>(event, "TruthMuons");
  auto my_truth_taus = Retrieve<xAOD::TruthParticleContainer>(event, "TruthTaus");
  auto my_truth_photons = Retrieve<xAOD::TruthParticleContainer>(event, "Truth3Photons");
  auto my_truth_neutrinos = Retrieve<xAOD::TruthParticleContainer>(event, "TruthNeutrinos");
  auto my_truth_tops = Retrieve<xAOD::TruthParticleContainer>(event, "TruthTop");
  auto my_truth_bosons = Retrieve<xAOD::TruthParticleContainer>(event, "TruthBoson");
  auto my_truth_BSM = Retrieve<xAOD::TruthParticleContainer>(event, "TruthBSM");

  std::cout << "\n\n New event\n\n" << std::endl;

  //MET
  std::cout << "non-interacting MET: mpx = " << my_truth_met->at(0)->mpx()/1000. << " GeV, mpy = " << my_truth_met->at(0)->mpy()/1000. << " GeV, sumet = " << my_truth_met->at(0)->sumet()/1000. << std::endl; 

  //Small R jets
  for (const auto &jet: *my_truth_jets) {
    std::cout << "Small R jet: pT = " << jet->p4().Pt()/1000. << " GeV, isbjet? " << jet->auxdata<int>("GhostBHadronsFinalCount") << std::endl;
  }
  
  //Large R jets 
  for (const auto &jet: *my_truth_largejets) {
    std::cout << "Large R jet: pT = " << jet->p4().Pt()/1000. << " GeV, tau1 = " << jet->auxdata<float>("Tau1_wta") << ", tau2 = " << jet->auxdata<float>("Tau2_wta") << ", tau3 = " << jet->auxdata<float>("Tau3_wta")  << std::endl;
  }

  //Electrons
  for (const auto &ele: *my_truth_electrons) {
    std::cout << "Electron: pT = " << ele->p4().Pt()/1000. << " GeV, dressed pT = " << ele->auxdata<float>("pt_dressed")/1000. << " GeV, classifierParticleOrigin = " << ele->auxdata<unsigned int>("classifierParticleOrigin") << ", motherID = " << ele->auxdata<int>("motherID") << ", etcone20 = " << ele->auxdata<float>("etcone20")/1000.  << " GeV, ptcone30 = " << ele->auxdata<float>("ptcone30")/1000. << " GeV " << std::endl;
  }
  
  //Muons
  for (const auto &mu: *my_truth_muons) {
    std::cout << "Muon: pT = " << mu->p4().Pt()/1000. << " GeV, dressed pT = " << mu->auxdata<float>("pt_dressed")/1000. << " GeV, classifierParticleOrigin = " << mu->auxdata<unsigned int>("classifierParticleOrigin") << ", motherID = " << mu->auxdata<int>("motherID") << ", etcone20 = " << mu->auxdata<float>("etcone20")/1000.  << " GeV, ptcone30 = " << mu->auxdata<float>("ptcone30")/1000. << " GeV " << std::endl;
  }

  //Taus
  for (const auto &tau: *my_truth_taus) {
    std::cout << "Tau: pT = " << tau->p4().Pt()/1000. << " GeV,  classifierParticleOrigin = " << tau->auxdata<unsigned int>("classifierParticleOrigin") << ", ishadronic tau ? " << (bool)tau->auxdata<char>("IsHadronicTau") << ", number of prongs = " << tau->auxdata<unsigned long>("numCharged") << std::endl; 
  }

  //Photons
  for (const auto &ph: *my_truth_photons) {
    std::cout << "Photon: pT = " << ph->p4().Pt()/1000. << " GeV,  classifierParticleOrigin = " << ph->auxdata<unsigned int>("classifierParticleOrigin") << ", motherID = " << ph->auxdata<int>("motherID") << std::endl; 
  }
  
  //Neutrinos
  TLorentzVector neutrinosum = TLorentzVector(0.,0.,0.,0.);
  for (const auto &nu: *my_truth_neutrinos) {
    std::cout << "Neutrino: pT = " << nu->p4().Pt()/1000. << " GeV " << std::endl;
    neutrinosum += nu->p4(); 
  }
  std::cout << "Neutrino sum px = " << neutrinosum.Px()/1000. << " GeV, sum py =  " << neutrinosum.Py()/1000. << " GeV (same as above ?) " <<  std::endl;

  //Top quarks
  for (const auto &top: *my_truth_tops) {
    std::cout << "Top quark: pT = " << top->p4().Pt()/1000. << " GeV,  classifierParticleOrigin = " << top->auxdata<unsigned int>("classifierParticleOrigin") << ", motherID = " << top->auxdata<int>("motherID") << ", daughterID = " << top->auxdata<int>("daughterID") << std::endl; 
  }

  //Bosons
  for (const auto &boson: *my_truth_bosons) {
    std::cout << "Boson: pT = " << boson->p4().Pt()/1000. << " GeV, pdgid = " << boson->auxdata<int>("pdgId") << ", classifierParticleOrigin = " << boson->auxdata<unsigned int>("classifierParticleOrigin") << ", motherID = " << boson->auxdata<int>("motherID") << ", daughterID = " << boson->auxdata<int>("daughterID") << std::endl;
  }

  //BSM
  for (const auto &bsm: *my_truth_BSM) {
    std::cout << "BSM particle: pT = " << bsm->p4().Pt()/1000. << " GeV, pdgid = " << bsm->auxdata<int>("pdgId") << std::endl;
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::postExecute()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dump_truth3_info::histFinalize()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
