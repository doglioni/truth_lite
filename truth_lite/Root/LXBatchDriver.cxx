//
// includes
//

#include <iostream>

#include <truth_lite/LXBatchDriver.h>

#include <cstdlib>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <TSystem.h>
#include <EventLoop/Job.h>
#include <RootCoreUtils/Assert.h>
#include <RootCoreUtils/ThrowMsg.h>

//
// method implementations
//

ClassImp(EL::LXBatchDriver)

namespace EL
{
  void LXBatchDriver ::
  testInvariant () const
  {
    RCU_INVARIANT (this != 0);
  }



  LXBatchDriver ::
  LXBatchDriver ()
  {
    RCU_NEW_INVARIANT (this);
  }



  void LXBatchDriver ::
  batchSubmit (const std::string& location, const SH::MetaObject& options,
           std::size_t njob) const
  {
    RCU_READ_INVARIANT (this);

    {
      std::ostringstream cmd;
      cmd << "cd " << location << "/submit";

      const std::string runFile = location + "/submit/run";
      const std::string namePrefix = options.castString("nc_jobName");
      bool dry_run = options.castBool("dry_run", false); 
      bool verbose = options.castBool("verbose", false); 

      int counter = 0;
      
      std::cout << "submit location: " << location << " # jobs: " << njob << std::endl;


      for (unsigned iter = 0, end = njob; iter != end; ++ iter) {
        std::string jobNameOpt = "";
	std::string logFileOpt = "";
        if (!namePrefix.empty()) {
          std::ostringstream tmp;
          tmp << " -J " << namePrefix << "_" << iter;
          jobNameOpt = tmp.str();  
	  tmp.clear();
	  tmp.str("");
	  tmp << " -o " << location << "/submit/" << namePrefix << "_" << iter << ".log";
	  logFileOpt = tmp.str();
        }

        cmd << " && bsub " 
            << options.castString (Job::optSubmitFlags)
            << jobNameOpt
	    << logFileOpt
            << " " << runFile << " " << iter;

	counter++;
	// execute command in batches of 10 bsub commands .. (too many will fail the shell)
	if (counter>10) {
	  if (verbose) std::cout << "going to execute: " << cmd.str() << std::endl;
	  if (!dry_run) {
	    if (gSystem->Exec (cmd.str().c_str()) != 0)
	      RCU_THROW_MSG (("failed to execute: " + cmd.str()).c_str());
	  }
	  // reset counter and cmd string
	  counter = 0;
	  cmd.clear();
	  cmd.str("");
	  cmd << "cd " << location << "/submit";
	}

      }

      // execute whatever is left in the cmd string
      if (counter > 0) {
	if (verbose) std::cout << "going to execute: " << cmd.str() << std::endl;
	if (!dry_run) { 	 
	  if (gSystem->Exec (cmd.str().c_str()) != 0)
	    RCU_THROW_MSG (("failed to execute: " + cmd.str()).c_str());
	}
      }
    }
  }
}
