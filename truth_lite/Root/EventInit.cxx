#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODRootAccess/TStore.h>

#include <CPAnalysisExamples/errorcheck.h>

#include <truth_lite/EventInit.h>
#include <truth_lite/CommonData.h>
#include <truth_lite/Util.h>

using namespace truth_lite;

static const char *APP_NAME = "EventInit";

// this is needed to distribute the algorithm to the workers
ClassImp(EventInit)

EventInit::EventInit(bool standalone)
: commonData(nullptr)
, standaloneMode(standalone)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  SetName("EventInit");

  if (standaloneMode) {
    Info("EventInit()", "Running in standalone mode");
  }
}

EL::StatusCode EventInit::setupJob(EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD();
  xAOD::Init("truth_lite").ignore(); // call before opening first file

  //StatusCode::enableFailure();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::histInitialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::fileExecute()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::changeInput(bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::initialize()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  stopWatchTotal.Start();
 
  commonData = new CommonData(config);

  eventCounter = 0;

  commonData->config.Print();

  bool isData = false;
  if (!standaloneMode)
    isData = !IsMC(wk());

  int isAF2 = isAtlfast;
  int isTRUTH = isTruthonly;

  commonData->outputULongs.push_back({"eventNumber", "event_number"});
  commonData->outputUInts.push_back({"runNumber", "run_number"});
  commonData->outputUInts.push_back({"lumiblock", "lumi_block"});
  commonData->outputUInts.push_back({"mcChannelNumber", "mc_channel_number"});

  commonData->outputFloats.push_back({"corrected_averageInteractionsPerCrossing", "av_int_per_xing"}); // corrected by PRW Tool
  commonData->outputFloats.push_back({"numberOfPrimaryVertices", "num_pv"});
  commonData->outputDoubles.push_back({"mc_event_weight", "mc_event_weight"});
  
  commonData->outputDoubles.push_back({"weight", "weight"});


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  static bool firstEvent = true;
  static bool startStopwatch = false;

  if (firstEvent) {
    firstEvent = false;
    startStopwatch = true;
  }
  
  bool isData = !IsMC(wk());

  if (startStopwatch) {
    // this is executed after the first event, i.e. on the second one
    // with this, we skip all the "lazy evaluation" like e.g. the egammaMVA
    // which is loaded in the first access.
    stopWatchExecute.Start();
    startStopwatch = false;
  }

  xAOD::TEvent *event = wk()->xaodEvent();

  try {
    if (eventPrintRate && (eventCounter % eventPrintRate) == 0) {
      Info("execute()", "Processing event %zu/%zu", eventCounter, (size_t)event->getEntries());
    }
    eventCounter++;

    commonData->clear();
    auto eventInfo = Retrieve<xAOD::EventInfo>(event, "EventInfo");

    std::pair<xAOD::EventInfo*, xAOD::ShallowAuxInfo*> shallowCopy = xAOD::shallowCopyObject(*eventInfo);
    commonData->eventInfo.reset(shallowCopy.first);
    commonData->eventAuxInfo.reset(shallowCopy.second);
    
    commonData->setWeight(1.); //will fix this soon.
    commonData->eventInfo->auxdata<double>("mc_event_weight") = 1.;
    
  }
  catch (std::runtime_error err) {
    Error("execute()", "Error: %s", err.what());
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::postExecute()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  commonData->clearTools();

  stopWatchTotal.Stop();
  std::cout << "Total time (including initialization): ";
  stopWatchTotal.Print();

  std::cout << "Event processing time (from first event): ";
  stopWatchExecute.Print();

  std::cout << "Total processed events: " << eventCounter << "\n";
  std::cout << "Rate: " << (eventCounter / stopWatchTotal.RealTime()) << " Hz (incl. initialization)\n";
  std::cout << "      " << ((eventCounter-1) / stopWatchExecute.RealTime()) << " Hz (excl. initialization)\n";
                                     // -1 because we skipped the first event, as many things are initialized there

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventInit::histFinalize()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
