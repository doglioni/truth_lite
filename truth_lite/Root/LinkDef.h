#include <truth_lite/CalcMT.h>

#include <truth_lite/dump_truth3_info.h>

#include <truth_lite/Setup_Truth_Objects.h>

#include <truth_lite/CommonData.h>
#include <truth_lite/EventInit.h>
#include <truth_lite/LXBatchDriver.h>
#include <truth_lite/NtupleOutput.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class truth_lite::GlobalConfig+;
#pragma link C++ class truth_lite::CommonData+;

#pragma link C++ class EL::LXBatchDriver+;

#pragma link C++ class truth_lite::EventInit+;
#pragma link C++ class truth_lite::NtupleOutput+;
#endif

#ifdef __CINT__
#pragma link C++ class truth_lite::Setup_Truth_Objects+;
#endif

#ifdef __CINT__
#pragma link C++ class truth_lite::dump_truth3_info+;
#endif

#ifdef __CINT__
#pragma link C++ class truth_lite::CalcMT+;
#endif
