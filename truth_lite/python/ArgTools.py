import os
import glob

WorkDir = os.getenv("WorkDir")

def _wd(*args):
	return os.path.join(WorkDir, *args)

def get_configs():
	files = glob.glob(_wd("truth_lite/share/config/*.py"))
	configs = map(lambda f: os.path.basename(f).replace(".py", ""), files)
	return configs

def get_samples():
	files = glob.glob(_wd("samples/*"))
	return map(os.path.basename, files)

