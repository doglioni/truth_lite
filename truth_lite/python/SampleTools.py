import sys
import os
import glob
from collections import namedtuple

from ROOT import *

from truth_lite.AmiEvents import AMI_EVENTS

OutputTypes = [
	'hist',
]

SampleId = namedtuple("SampleId", "name config syst title")
Sample = namedtuple("Sample", "data hist name config syst title")


def input_path(sample, validate=False):
	workDir = os.getenv("WorkDir")
	path = os.path.join(workDir, "samples", sample)

	if validate and not os.path.exists(path):
		print "Error: Failed to find sample '%s' in '%s'" % (sample, path)
		sys.exit(1)

	return path

def output_path(sample, config, syst=None, validate=False, create_base=False):
	workDir = os.getenv("WorkDir")
	path = os.path.join(workDir, "output", config)
	if syst:
		path = os.path.join(path, syst)

	# if create_base is set, create the base path for the sample
	if create_base and not os.path.exists(path):
		os.makedirs(path)

	path = os.path.join(path, sample)
	if validate and not os.path.exists(path):
		print "Error: Failed to find output path for sample '%s' in '%s'" % (sample, path)
		sys.exit(1)

	return path

def result_path(sample, config, smp_type, syst=None, validate=False):
	path = output_path(sample, config, syst)
	path = os.path.join(path, smp_type)

	if validate and not os.path.exists(path):
		print "Error: Failed to find output sample of type '%s' for sample '%s' in '%s'" % (smp_type, sample, path)
		sys.exit(1)

	return path

def load(path):
	sh = SH.SampleHandler()
	sh.load(path)
	return sh

def load_output_sample(data_type, sample, config, syst=None, title=None):
	"""
	Load and return 
	"""
	path = output_path(sample, config, syst)
	if not os.path.exists(path):
		print "Failed to load sample '%s', path not found: %s" % (sample, path)
		return False

	sh_hist = SH.SampleHandler()
	sh_hist.load(os.path.join(path, "hist"))

	if not title:
		title = sh_hist.at(0).getMetaString("title", "")

	if data_type == "hist":
		return Sample(
			data=None, hist=sh_hist,
			name=sample, config=config,
			syst=syst, title=title,
		)

	sh_data = SH.SampleHandler()
	sh_data.load(os.path.join(path, "output-" + data_type))
	sh_data.setMetaString("nc_tree", "CollectionTree")

	return Sample(
		data=sh_data, hist=sh_hist,
		name=sample, config=config,
		syst=syst, title=title,
	)

def load_output_sample_from_id(sample, data_type="hist"):
	if not data_type:
		data_type = "hist"

	return load_output_sample(data_type, sample.name, sample.config, sample.syst, sample.title)


def sample_id(sample):
	syst_part = "_" + sample.syst if sample.syst else ""

	return "%s%s_%s" % (sample.config, syst_part, sample.name)

def total_events(sub_sample):
	"""
	Return n_events before preselection
	"""
	import truth_lite.ext.termcolor as tc


	skimHist = sub_sample.readHist("DerivationStat_Weights")
	n_tot = skimHist.GetBinContent(1)

	evtHist = sub_sample.readHist("DerivationStat_Events")
	n_evt = int(evtHist.GetBinContent(1))

	chid = get_mc_id(sub_sample)
	if chid in AMI_EVENTS and AMI_EVENTS[chid] != n_evt:
		sf = AMI_EVENTS[chid] / float(n_evt)
		n_tot *= sf
		tc.cprint("Warning: incorrect total number of events for %s, correcting by factor %f" % (sub_sample.name(), sf), "yellow", file=sys.stderr)

	if n_tot < 1:
		# the dataset was produced without skimming, we can just
		# use the normal cutflow, as the "without cut" stage contains
		# all events of the data set
		cutflow = sub_sample.readHist("passedWeights_el_Preselection")
		n_tot = cutflow.GetBinContent(1)

	return n_tot

def sample_weight(sub_sample):
	is_data = sub_sample.getMetaDouble("data", 0.)
	if is_data:
		return 1.0

	xs = sub_sample.getMetaDouble("xs")
	if xs == 0:
		print "Warning: XS not set for sub sample", sub_sample.name()
		xs = 1

	total = total_events(sub_sample)

	return xs / total

def get_luminosity(sample):
	total_lumi = 0

	unknown_samples = []
	for sub_sample in sample.hist:
		lumi = sub_sample.getMetaDouble("lumi", -1)
		if lumi == -1:
			unknown_samples.append(sub_sample)
			continue
		total_lumi += lumi

	if len(unknown_samples) == sample.hist.size():
		print "Now luminosity information for", sample.name
	elif len(unknown_samples) > 0:
		print "Missing luminosity information for some sub-samples, the total will be wrong!"
		print "Datasets with missing info:", ", ".join([s.name() for s in unknown_samples])

	return total_lumi


def read_hist(sample, hist_name, weighted=True):
	sh = sample.hist
	hist = None

	missing_samples = []

	for i in xrange(sh.size()):
		sub_sample = sh.at(i)

		sub_hist = sub_sample.readHist(hist_name)
		if not sub_hist:
			missing_samples.append(sub_sample.name())
			continue

		if weighted:
			wgt = sample_weight(sub_sample)
			sub_hist.Scale(wgt)

		if not hist:
			hist = sub_hist.Clone()
		else:
			hist.Add(sub_hist)

	if hist != None and len(missing_samples) > 0:
		print "Failed to load '%s' for some sub-samples:" % hist_name
		for ms in missing_samples:
			print "", ms

	return hist

def cross_section(sample, cut_weight):
	import truth_lite.HistTools as HT

	hist = HT.draw_hist(sample, "1.0", cut_weight=cut_weight, hist_name="xs_" + sample_id(sample), binning="1,0,2", weighted=True)

	err = Double()
	xs = hist.IntegralAndError(0, hist.GetNbinsX()+1, err)

	return xs, err

def get_mc_id(sample):
	parts = sample.name().split(".")
	p0 = 0
	for i, part in enumerate(parts):
		if part.startswith("mc") or part.startswith("data"):
			p0 = i
			break
	chid = parts[p0+1]
	return int(chid)

def _cfg_syst(string, def_cfg, def_syst):
	if "#" not in string:
		config = string
		syst = def_syst
	else:
		parts = string.split("#")
		config = parts[0]
		syst = parts[1]

	if not config or config == "":
		config = def_cfg

	if not syst or syst == "":
		syst = def_syst

	return config, syst

def parse_sample_id(opts, smp_str):
	"""
	Return a SampleId

	Deconstruct a string in the form "[config#syst:]name[:title]" that
	fully qualifys a sample. If the config is not specified, opts.config
	is taken, if the title is not specified it is set to None. A Sample
	object is returned, that stores the results.
	"""
	def_config = None
	def_syst = None
	if opts:
		def_config = opts.config if opts.config else None
		def_syst = opts.syst if ("syst" in opts and opts.syst) else None

	if ":" not in smp_str:
		return SampleId(name=smp_str, config=def_config, syst=def_syst, title=None)

	parts = smp_str.split(":")
	config, syst = _cfg_syst(parts[0], def_config, def_syst)

	if len(parts) == 2:
		return SampleId(name=parts[1], config=config, syst=syst, title=None)
	elif len(parts) == 3:
		return SampleId(name=parts[1], config=config, syst=syst, title=parts[2])
	else:
		print "Error: Invalid sample string '%s'" % smp_str
		print "       Expected [config:]sample[:title]"
		raise Exception("Invalid sample string")

def parse_sample_ids(opts):
	"""
	Parse and replace all elements in opts.samples

	This is a helper function that calls parse_sample for all
	elements in opts.samples and replaces the list by the results.
	The original list is stored as opts.orig_samples.
	"""
	result = []
	for smp in opts.samples:
		result.append(parse_sample(opts, smp))

	opts.orig_samples = opts.samples
	opts.samples = result

def parse_and_load_samples(opts, data_type="hist"):
	"""
	Parse and replace all elements in opts.samples
	"""
	result = []

	for smp in opts.samples:
		smp_id = parse_sample_id(opts, smp)

		smp_obj = load_output_sample_from_id(smp_id, data_type)
		if not smp_obj:
			print "Error: Failed to load sample %s" % sample_id(smp_id)
			sys.exit(1)

		result.append(smp_obj)

	opts.orig_samples = opts.samples
	opts.samples = result


def add_output_sample_opts(parser, sample_switch=False):
	parser.add_argument("--config", "-c", required=True, help="The configuration of the sample")
	parser.add_argument("--syst", help="The systematic of the sample")

	if sample_switch:
		parser.add_argument("--sample", "-s", help="The sample")
	else:
		parser.add_argument("sample", help="The sample")

def process_output_sample_opts(opts):
	opts.sample_id = parse_sample_id(opts, opts.sample)
	opts.sample_path = output_path(opts.sample_id.name, opts.sample_id.config, opts.sample_id.syst, validate=True)

def add_result_sample_opts(parser, sample_switch=False):
	parser.add_argument("--config", "-c", help="The configuration of the sample to retrieve")
	parser.add_argument("--syst", help="The systematic of the sample")
	parser.add_argument("--tree", "-t", default="ntuple", help="The output ntuple to use")
	if sample_switch:
		parser.add_argument("--sample", "-s", help="The sample")
	else:
		parser.add_argument("sample", help="The sample")

def process_result_sample_opts(opts, load=False):
	opts.sample_id = parse_sample_id(opts, opts.sample)
	opts.sample_path = output_path(opts.sample_id.name, opts.sample_id.config, opts.sample_id.syst, validate=True)

	if load:
		opts.sample = load_output_sample_from_id(opts.sample_id, opts.tree)
