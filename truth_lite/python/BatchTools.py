import sys
import os

BATCH_SYSTEMS = [
	'lxbatch', 'wtal','gva','slac',
]

GROUPDISK = {
	'lxbatch': "CERN-PROD_LOCALGROUPDISK",
	'wtal': "WUPPERTALPROD_LOCALGROUPDISK",
	'gva': "UNIGE-DPNC_LOCALGROUPDISK",
	'slac': "SLACXRD_LOCALGROUPDISK",
}

LOCAL_PATH = {
	'lxbatch': ("srm://srm-eosatlas.cern.ch/", "root://eosatlas//"),
	'wtal': ("srm://grid-se.physik.uni-wuppertal.de/","dcap://grid-se.physik.uni-wuppertal.de:22125//"),
	'gva': ("srm://grid05.unige.ch/","root://grid05.unige.ch//"),
	'slac': ("/atlas","root://atlprf01.slac.stanford.edu:11094/"),
}

def submit(command, queue, opts):
	syst = detect()

	from os import system

	if "dry_run" in opts and opts.dry_run:
		def system(cmd):
			pass	

	# extract sampleName, configName, and jobID
	tmp, jobID = command.split(' ')
	tmp2 = (tmp.replace('/submit/run', '')).split('/')
	sampleName = tmp2[-1]
	configName = tmp2[ len(tmp2)-2 ]
	submitDir = tmp.replace('/run','')
	# print configName, ", ", sampleName, ", ", jobID
	jobName = "%s_%s_%s" % (configName, sampleName, jobID)
	logFile = "%s/%s_%s_%s.log" % (submitDir, configName, sampleName, jobID)
	jobOpt  = " -J %s" % (jobName)
	logOpt  = " -o %s" % (logFile)

	if os.access(logFile,os.R_OK): system("mv -f "+logFile+" "+logFile+".old")

	batch = ""
	if not opts.batch is None: batch = opts.batch

	if syst == "lxbatch":
		cmd = "bsub -L /bin/bash -q {queue} {jobName} {logFile} {opts} {cmd}".format(queue=queue, jobName=jobOpt, logFile=logOpt, opts=batch, cmd=command)
	elif syst == "wtal":
		cmd = "qsub {opts} {cmd}".format(opts=batch, cmd=command)
	elif  syst == "gva":
		cmd = "qsub -q {queue} -l vmem=3500mb -t {jid} {opts} {cmd}".format(queue=queue, jid=jobID, opts=batch, cmd=tmp)
	elif syst=='slac':
		cmd = "bsub -q medium {cmd}".format(cmd=command)

	if "verbose" in opts and opts.verbose: print cmd

	system(cmd)

def detect(silent=False):
	import platform
	hostname = platform.node()

	if hostname.endswith("cern.ch"):
		if not silent:
			print "Auto detect batch system: LXBatch"
		return 'lxbatch'

	if "uni-wuppertal.de" in hostname:
		if not silent:
			print "Auto detect batch system: Wuppertal"
		return 'wtal'

	if "atlas" in hostname:
		if not silent:
			print "Auto detect batch system: Geneva"
		return 'gva'
	
	if "atlint" in hostname:
		if not silent:
		        print "Auto detect batch system: SLAC"
	        return 'slac'

	print "Error: Failed to detect batch system, please use the --batch option"
	return None

def default_queue(bsys):
	if bsys == "lxbatch":
		return "1nh"
	return None

