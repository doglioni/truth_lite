#ifndef truth_lite_dump_truth3_info_H
#define truth_lite_dump_truth3_info_H

#include <EventLoop/Algorithm.h>

namespace truth_lite {

class CommonData;

class dump_truth3_info : public EL::Algorithm
{
  CommonData* commonData; //!

public:

  // this is a standard constructor
  dump_truth3_info ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(dump_truth3_info, 1);
};

}

#endif
