#ifndef SWUP_EVENT_DATA_H
#define SWUP_EVENT_DATA_H

#include <TObject.h>

#include <xAODBase/IParticleContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMuon/MuonContainer.h>
#include "xAODTruth/TruthParticleContainer.h"

#include <xAODRootAccess/TStore.h>
#include <xAODRootAccess/TEvent.h>

#include <truth_lite/Util.h>

namespace truth_lite {

namespace Output {

struct Decoration
{
	Decoration(const std::string& auxvar, const std::string& outname, const char& type = 'F') : auxvar(auxvar), outname(outname), type(type) {}
	std::string auxvar; // variable name of the decoration (in transient store)
	std::string outname; // name for this decoration in the output tree (ntuple)
	char type; // type of variable, this is needed for the setting the proper branch type in the output tree.
};

struct Particle
{
	std::string containerName;
	std::string prefix;
	std::vector< Decoration > decorations;
};

struct Met
{
	std::string container;
	std::string name;
	std::string prefix;
	bool onlyMet;
};

} // Output namespace

struct GlobalConfig
{
	// allow algorithms to output default values or fallback from signal to baseline objects
	bool permissive = false;

	void Print() const
	{
		std::cout << "truth_lite - GlobalConfig\n";
		std::cout << "-------------------\n";
		std::cout << "permissive: " << permissive << "\n";
		std::cout << "\n";
	}
};

/** 
 * The CommonData object holds pointers to data used by all or most
 * algorithms. The EventInit algorithm owns the CommonData object
 * and provides an accessor function.
 */
struct CommonData : public TObject
{
	// common tools

	// will be filled by the EventInit algorithm
	const GlobalConfig config;

	// additional output fields
	// these have to be initialized before the Output algorithm
	// is initialized

	// pair of (auxvar name, output branch name)

	// output for <double> decorations
	std::vector<std::pair<std::string, std::string>> outputDoubles;

	// output for <float> decorations
	std::vector<std::pair<std::string, std::string>> outputFloats;

	// output for <int> decorations
	std::vector<std::pair<std::string, std::string>> outputInts;

	// output for <uint32_t> decorations
	std::vector<std::pair<std::string, std::string>> outputUInts;

	// output for <unsigned long long> decorations
	std::vector<std::pair<std::string, std::string>> outputULongs;
	
	std::vector<Output::Particle> outputParticles;
	std::vector<Output::Met> outputMet;

	// per event data
	double weight;

	std::unique_ptr<xAOD::EventInfo> eventInfo;
	std::unique_ptr<xAOD::ShallowAuxInfo> eventAuxInfo;

	const xAOD::JetContainer *fatJets;
	ShallowCopy<xAOD::JetContainer> calibFatJets;
	xAOD::JetContainer *signalFatJets;

	const xAOD::JetContainer *jets;
	ShallowCopy<xAOD::JetContainer> calibJets;
	ShallowCopy<xAOD::JetContainer> calibLargeJets;
	xAOD::JetContainer *baseJets;
	xAOD::JetContainer *signalJets;
	xAOD::JetContainer *bJets;

	const xAOD::ElectronContainer *electrons;
	ShallowCopy<xAOD::ElectronContainer> calibElectrons;
	xAOD::ElectronContainer *signalElectrons;
	xAOD::ElectronContainer *baseElectrons;

	const xAOD::TruthParticleContainer *telectrons;
        ShallowCopy<xAOD::TruthParticleContainer> calibtElectrons;
	xAOD::TruthParticleContainer *signaltElectrons;
	xAOD::TruthParticleContainer *basetElectrons;

	const xAOD::MuonContainer *muons;
	ShallowCopy<xAOD::MuonContainer> calibMuons;
	xAOD::MuonContainer *signalMuons;
	xAOD::MuonContainer *baseMuons;

	const xAOD::TruthParticleContainer *tmuons;
        ShallowCopy<xAOD::TruthParticleContainer> calibtMuons;
	xAOD::TruthParticleContainer *signaltMuons;
	xAOD::TruthParticleContainer *basetMuons;

	xAOD::IParticleContainer *signalLeptons;

	std::pair<xAOD::MissingETContainer*, xAOD::MissingETAuxContainer*> met;
	xAOD::MissingET *metRefFinal = nullptr;

	CommonData(const GlobalConfig& cfg)
	: config(cfg)
	{ 
		clear();
	}

	// virtual destructor for TObject/ClassDef compatibility
	virtual ~CommonData()
	{ }

	void clear()
	{
		weight = 1.0;

		eventInfo.reset();
		eventAuxInfo.reset();

		fatJets = nullptr;
		calibFatJets.first.reset();
		calibFatJets.second.reset();
		signalFatJets = nullptr;

		jets = nullptr;
		calibJets.first.reset();
		calibJets.second.reset();
		signalJets = nullptr;
		bJets = nullptr;

		electrons = nullptr;
		calibElectrons.first.reset();
		calibElectrons.second.reset();
		signalElectrons = nullptr;
		baseElectrons = nullptr;


		muons = nullptr;
		calibMuons.first.reset();
		calibMuons.second.reset();
		signalMuons = nullptr;
		baseMuons = nullptr;

		signalLeptons = nullptr;
	}

	void setWeight(double wgt)
	{
		weight = wgt;
		if (eventInfo)
			eventInfo->auxdata<double>("weight") = weight;
	}

	void clearTools()
	{
	  //delete susyTool;
	  //susyTool = nullptr;

	}

	void addDecorations(const std::string& containerOrPrefix, const std::vector<Output::Decoration>& decorations)
	{
		for (auto& co: outputParticles) {
			if (co.containerName == containerOrPrefix || co.prefix == containerOrPrefix) {
				co.decorations.insert(co.decorations.end(), decorations.begin(), decorations.end());
				break;
			}
		}
	}

	ClassDef(CommonData, 1);
};

}

#endif //SWUP_EVENT_DATA_H
