#ifndef truth_lite_EventInit_H
#define truth_lite_EventInit_H

#include <EventLoop/Algorithm.h>
#include <TStopwatch.h>
#include <truth_lite/CommonData.h>

namespace truth_lite {

class CommonData;

/**
 * The EventInit algorithm is the only algorithm that has to be executed
 * before all other algorithms. It provides a handle to the CommonData
 * object (see getCommonData()) and configures the common tools.
 * In addition, it handles some basic eventCounter printout and the
 * performance monitoring.
 *
 * To access the EventInit algorithm, use
 *     EventInit *eventInit = dynamic_cast<EventInit*>(wk()->getAlg("EventInit"));
 * in the initialize() function of your algorithm. The CommonData object
 * can then be accessed via
 *     CommonData *commonData = eventInit->getCommonData();
 */
class EventInit : public EL::Algorithm
{
private:
  // this has to be a pointer, as the header files needed
  // for the class declaration have to be protected from
  // CINT
  CommonData *commonData; //!
  size_t eventCounter; //!
  TStopwatch stopWatchTotal; //!
  TStopwatch stopWatchExecute; //!

  bool standaloneMode; //!

public:

  bool isAtlfast;
  bool isTruthonly = false;
  bool provideTools = true;

  int jesNPSet = 0;

  size_t eventPrintRate = 500;  
  int puDefaultChannel = 410000;

  std::string stElBaseline = "LooseLH";
  std::string stElSignal = "TightLH";

  std::string stMuSignal = "Medium";

  std::string stMuIsoWP = "GradientLoose";
  std::string stElIsoWP = "GradientLoose";
  std::string stPhIsoWP = "Cone40";
  bool stRequireIsoSignal = true;

  GlobalConfig config;

  // this is a standard constructor
  EventInit (bool standalone = false);

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  CommonData* getCommonData()
  {
    return commonData;
  }

  void setStandalone()
  {
    standaloneMode = true;
  }

  // this is needed to distribute the algorithm to the workers
  ClassDef(EventInit, 1);
};

}

#endif
