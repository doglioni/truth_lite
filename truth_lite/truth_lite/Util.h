#ifndef SWUP_UTIL_H
#define SWUP_UTIL_H

#include <functional>
#include <iostream> // needed for ShallowCopy...
#include <memory>

#include <boost/throw_exception.hpp>

#include <xAODCore/ShallowCopy.h>
#include <EventLoop/Worker.h>

#include <Math/GenVector/VectorUtil.h>


#ifdef CHECK
#undef CHECK
#endif
#define CHECK(_Expr)                                 \
    do {                                             \
        bool result_M_ = _Expr;                      \
        if (!result_M_) {                            \
            Error(APP_NAME, "'%s' failed!", #_Expr); \
            return EL::StatusCode::FAILURE;          \
        }                                            \
    } while (0)

#define CHECK_THROW(_Expr)                                                              \
    do {                                                                                \
        bool result_M_ = _Expr;                                                         \
        if (!result_M_) {                                                               \
            BOOST_THROW_EXCEPTION(std::runtime_error(std::string(#_Expr) + " failed!")); \
        }                                                                               \
    } while (0)

namespace truth_lite {

// decorations handled entirely in GIT code, can use <int> type which is better for dumping into mini-ntuple
static SG::AuxElement::Decorator<int> dec_loosebad("loosebad");
static SG::AuxElement::Decorator<int> dec_tightbad("tightbad");
static SG::AuxElement::Decorator<int> dec_pileup("pileup");
static SG::AuxElement::Decorator<int> dec_hotcell("hotcell");
static SG::AuxElement::Decorator<int> dec_bjet60("bjet60");
static SG::AuxElement::Decorator<int> dec_bjet70("bjet70");
static SG::AuxElement::Decorator<int> dec_bjet77("bjet77");
static SG::AuxElement::Decorator<int> dec_bjet85("bjet85");
static SG::AuxElement::Decorator<int> dec_bjet_OR("bjetOR");
static SG::AuxElement::Decorator<int> dec_muon_bad("badmuon");
static SG::AuxElement::Decorator<int> dec_muon_cosmic("cosmicmuon");
static SG::AuxElement::Decorator<int> dec_truthLabel("truthLabel");
static SG::AuxElement::Decorator<int> dec_truthHadLabel("truthHadLabel");

static SG::AuxElement::Decorator<float> dec_d0sig("d0sig");
static SG::AuxElement::Decorator<float> dec_z0("z0");
static SG::AuxElement::Decorator<float> dec_deltaR_bb("deltaRbb");

static SG::AuxElement::Decorator<int> dec_isoLooseTrackOnly("isoLooseTrackOnly");
static SG::AuxElement::Decorator<int> dec_isoLoose("isoLoose");
static SG::AuxElement::Decorator<int> dec_isoTight("isoTight");
static SG::AuxElement::Decorator<int> dec_isoGradient("isoGradient");
static SG::AuxElement::Decorator<int> dec_isoGradientLoose("isoGradientLoose");

static SG::AuxElement::Decorator<int> dec_motherPdgId("motherPdgId");
static SG::AuxElement::Decorator<int> dec_statusCode("statusCode");

static SG::AuxElement::Decorator<int> dec_PartonTruthLabelID("PartonTruthLabelID");

static SG::AuxElement::Decorator<char> dec_bad("bad");
static SG::AuxElement::Decorator<char> dec_baseline("baseline");
static SG::AuxElement::Decorator<char> dec_signal("signal");
static SG::AuxElement::Decorator<char> dec_cosmic("cosmic");
static SG::AuxElement::Decorator<char> dec_passOR("passOR");

static SG::AuxElement::Accessor<float>  acc_jvt("Jvt");
static SG::AuxElement::ConstAccessor<float> cacc_jvt("Jvt");
static SG::AuxElement::Decorator<double> dec_effscalefact("effscalefact");


static const double MeV = 1.0;
static const double GeV = 1e3 * MeV;
static const double TeV = 1e3 * GeV;

template <typename Container>
using ShallowCopy = std::pair<std::unique_ptr<Container>, std::unique_ptr<xAOD::ShallowAuxContainer>>;

template <typename T, typename Store>
const T* Retrieve(Store* st, const char *name)
{
  const T* result = nullptr;
  CHECK_THROW(st->retrieve(result, name));
  return result;
}

template <typename T, typename Store>
const T* Retrieve(Store* st, const std::string& name)
{
  return Retrieve<T>(st, name.c_str());
}

template <typename T, typename Store>
T* MutableRetrieve(Store *st, const std::string& name)
{
  T* result = nullptr;
  CHECK_THROW(st->retrieve(result, name));
  return result;
}

template <typename Store, typename Container, typename AuxContainer>
void Record(Store* st, std::pair<Container*, AuxContainer*> container, const char *name)
{
  CHECK_THROW(st->record(container.first, name));
  CHECK_THROW(st->record(container.second, std::string(name) + "Aux."));
}

template <typename T>
ShallowCopy<T> SaveShallowCopy(const T& cont)
{
  auto res = xAOD::shallowCopyContainer(cont);

  return std::make_pair(std::unique_ptr<T>(res.first), std::unique_ptr<xAOD::ShallowAuxContainer>(res.second));
}

/**
 * Create a container and the accomp. aux. container, and connect the two.
 */
template <typename Container, typename AuxContainer>
std::pair<Container*, AuxContainer*> MakeContainer()
{
  Container *cnt = new Container();
  AuxContainer *aux = new AuxContainer();
  cnt->setStore(aux);
  return std::make_pair(cnt, aux);
}

/**
 * Copy object pointers from a shallow copy to a view container, if they fulfill some predicate p.
 */
template <typename T, typename ShallowContainer, typename TargetViewContainer>
void CopyViewIf(ShallowContainer& from, TargetViewContainer to, const std::function<bool (const T& particle)>& pred)
{
  // don't use auto in the for loop, the container returns something that is
  // convertible to T*, but not T* so the const_cast later fails
  for (const T* particle: *from.first) {
    if (pred(*particle)) {
      to->push_back(const_cast<T*>(particle));
    }
  }
}

/**
 * Copy object pointers from a view container to a view container, if they fulfill some predicate p.
 */
template <typename T, typename ViewContainer>
void CopyViewFromViewIf(ViewContainer& from, ViewContainer& to, const std::function<bool (const T& particle, size_t index)>& pred)
{ 
  // don't use auto in the for loop, the container returns something that is
  // convertible to T*, but not T* so the const_cast later fails
  size_t index = 0;
  for (const T* particle: *from) {
    if (pred(*particle, index)) {
      to->push_back(const_cast<T*>(particle));
    } 
    index++;
  }
}

inline bool ComparePt(const xAOD::IParticle* a, const xAOD::IParticle* b)
{
  return a->pt() > b->pt();
}

inline const xAOD::TruthParticle* GetLastTruthParticle(const xAOD::TruthParticle* particle)
{
  auto pdgId = particle->pdgId();

  if (particle->nChildren() == 1 && particle->child(0)->pdgId() == pdgId)
    return GetLastTruthParticle(particle->child(0));

  return particle;
}

inline const xAOD::TruthParticle* FindDecayProduct(const xAOD::TruthParticle* particle, std::function<bool (const xAOD::TruthParticle*)> pred)
{
  for (size_t i=0; i < particle->nChildren(); ++i) {
    auto child = particle->child(i);

    if (pred(child)) {
      return child;
    }
  }

  return nullptr;
}

inline float DeltaPhi(float phi1, float phi2)
{
  return ROOT::Math::VectorUtil::Phi_mpi_pi(phi1 - phi2);
}

inline double GetBWeight(const xAOD::Jet& jet)
{
  if (!jet.btagging()) return jet.p4().Pt(); //for truth jets, there is no b-tagging object.
  double bweight = 0;
  jet.btagging()->MVx_discriminant("MV2c20", bweight);
  return bweight;
}

inline bool IsMC(const xAOD::EventInfo *eventInfo)
{
  return eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);
}

inline bool IsMC(xAOD::TEvent* event)
{
  auto eventInfo = Retrieve<xAOD::EventInfo>(event, "EventInfo");
  return IsMC(eventInfo);
}

inline bool IsMC(EL::Worker *wk)
{
  xAOD::TEvent* event = wk->xaodEvent();
  return IsMC(event);
}

}

#endif //SWUP_UTIL_H
