# sample_groups.common.py
# Definition of common sample groups, accessed by the GroupTools module.
# Groups are defined by their name and a list of (sub-)samples that
# belong to the group. Use "<sample>#<pattern>" to specify a regular
# expression by which the sub-samples are selected. If no pattern is
# given, all sub-samples are taken. The sample name itself is also a
# regular expression.

# Use this file to specifiy groups that are commonly used by many 
# people. For your own definitions, you can edit the file
# truth_lite/share/sample_groups.local.py
# which is not part of the git repository and remains local to you.

# this is just an example and will _not_ be used by truth_lite.
EXAMPLE_SampleGroups = {
	# basic format:
	'group_name': ['list', 'of', 'subsamples'],
	# this defines a group called "group_name", consisting of
	# all subsamples of the samples "list", "of" and "subsamples".

	# one can define a pattern to match only some subsamples
	'wenu': ['wjets#.*W.*enu.*'],
	# you have to use a regular expression, to match any number of
	# any characters, use ".*" (. is any character, * means the previous
	# character any times)

	# regular expressions can also be used to define the sample
	'data': ['data_.*'],
	# creates a group of all samples matching the pattern "data_.*"
}


SampleGroups = {
    # all top quark processes
    'top_50ns': ['mc15*PowhegPythiaEvtGen*'],
    'Wjets': ['mc15*Sherpa*W*Pt*'],
    'Zjets': ['mc15*Sherpa*Z*Pt*'],
    'VV'   : ['user.eifert.mc15_13TeV.*.Sherpa_CT10*', 'mc15_13TeV.*.Sherpa_CT10_ll*', 'mc15_13TeV.*.Sherpa_CT10_ggll*' ,'mc15_13TeV.*.Sherpa_CT10_W*Zl*', 'mc15_13TeV.361086.Sherpa_CT10_ZqqZll*'],
    'dijets': ['user.eifert.mc15*Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*'],
    'ttZ'  : ['mc15*MadGraphPythia8EvtGen_A14NNPDF23LO_ttZnnqq*'],

    'powheg_W': ['user.eifert.mc15*PowhegPythia8EvtGen_AZNLOCTEQ6L1_W*'],
    'powheg_Z': ['user.eifert.mc15*PowhegPythia8EvtGen_AZNLOCTEQ6L1_Z*'],
    'data': ['data15*'],
    'MadGraph_W': ['MGW*'],
    'Powheg_W':['PHW*'],
    'Sherpa_W':['SH_W*'],
    'Sherpa_D':['SH_Di*'],
    'data_50ns':['data_27*']
    }

