eventInit = truth_lite.EventInit()

eventInit.isAtlfast = False
eventInit.eventPrintRate = 100

eventInit.config.permissive = True

doSyst = False

ntupleWriter = truth_lite.NtupleOutput()

outputStream = EL.OutputStream(ntupleWriter.outputName)
ntupleSvc = EL.NTupleSvc(ntupleWriter.outputName)

#print_jets = truth_lite.print_jets()
#Setup_Truth_Objects = truth_lite.Setup_Truth_Objects()
#dump_truth3_info = truth_lite.dump_truth3_info()
#CalcMT=truth_lite.CalcMT()

execute_extra_args()

Algorithms = [
	ntupleSvc,
	eventInit,
	#print_jets,
	#Setup_Truth_Objects,
	#dump_truth3_info,
	#CalcMT,
]

if doSyst:
	Algorithms.append(sysVar)

Algorithms += [

	ntupleWriter,
]

Output = [
	outputStream
]
