###############################################################################
# presel 4 jets (with pt cuts)

def cut_presel_4j(commonData, cutflow, weight):
	jet_pt_cuts = [25*GeV, 25*GeV, 25*GeV, 25*GeV]

	if commonData.signalJets.size() < 4:
		return 0

	for i, ptcut in enumerate(jet_pt_cuts):
		if commonData.signalJets.at(i).pt() < ptcut:
			return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("presel_4j", cut_presel_4j, [
	"4 jets > 25 GeV",
])

##############
# presel 4 jets and at least one b-jet

def cut_presel_4j_1b(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("presel_4j") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
	  return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("presel_4j_1b", cut_presel_4j_1b, [
	"presel_4j",
	"nb >= 1",
])




###############
# presel 4 jets and at least two b-jets

def cut_presel_4j_2b(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("presel_4j") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.size() < 2:
	  return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("presel_4j_2b", cut_presel_4j_2b, [
	"presel_4j",
	"nb >= 2",
])

###############
# presel 4 jets and no b-jets

def cut_presel_4j_0b(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("presel_4j") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
	  return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("presel_4j_0b", cut_presel_4j_0b, [
	"presel_4j",
	"nb = 0",
])


