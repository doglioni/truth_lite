###############################################################################
# tN_med_SR

add_virtual_cut_ex("tN_med_SR", [
	Cut("nJets", "nJets >= 4 (25 GeV)", lambda data: n_jets_pt_larger(data, 4, 25*GeV)),
	Cut("jet1pT", "pT_jet1 > 80 GeV", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 80*GeV))),
	Cut("jet2pT", "pT_jet2 > 60 GeV", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >= 60*GeV))),
	Cut("jet3pT", "pT_jet3 > 40 GeV", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >= 40*GeV))),
	Cut("nbTag", "n btags >= 1", lambda data: data.bJets.size() >= 1),
	Cut("dPhi_jet_met", "dPhi_jet2_met >= 0.8", lambda data: (data.signalJets.size() >= 2) and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) >= 0.8),
	Cut("met", "met >= 200 GeV", lambda data: data.metRefFinal.met() >= 200*GeV),
	Cut("HT", "HTSig >= 12.5", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 12.5),
	Cut("mT", "mT >= 140 GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 140*GeV),
	Cut("m_hadTop", "m_hadTop in [130,195] GeV", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 195)),
	Cut("aMT2", "aMT2 >= 170 GeV", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 170),
	], save_sub_results=True)


###############
# tN_med TCR

add_virtual_cut_ex("tN_med_TCR", [
	Cut("nJets", "nJets >= 4 (25 GeV)", lambda data: n_jets_pt_larger(data, 4, 25*GeV)),
	Cut("jet1pT", "pT_jet1 > 80 GeV", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 80*GeV))),
	Cut("jet2pT", "pT_jet2 > 60 GeV", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >= 60*GeV))),
	Cut("jet3pT", "pT_jet3 > 40 GeV", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >= 40*GeV))),
	Cut("nbTag", "n btags >= 1", lambda data: data.bJets.size() >= 1),
	Cut("dPhi_jet_met", "dPhi_jet2_met >= 0.8", lambda data: (data.signalJets.size() >= 2) and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) >= 0.8),
	Cut("met", "met >= 200 GeV", lambda data: data.metRefFinal.met() >= 200*GeV),
	Cut("HT", "HTSig >= 12.5", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 12.5),
	Cut("mT", "mT in [60,90] GeV", lambda data: (data.eventInfo.auxdata(float)("mT") >= 60*GeV and data.eventInfo.auxdata(float)("mT") <= 90*GeV)),
	Cut("m_hadTop", "m_hadTop in [130,195] GeV", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 195)),
	Cut("aMT2", "aMT2 >= 120 GeV", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 120),
	])

###############
# tN_med WCR

add_virtual_cut_ex("tN_med_WCR", [
	Cut("nJets", "nJets >= 4 (25 GeV)", lambda data: n_jets_pt_larger(data, 4, 25*GeV)),
	Cut("jet1pT", "pT_jet1 > 80 GeV", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 80*GeV))),
	Cut("jet2pT", "pT_jet2 > 60 GeV", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >= 60*GeV))),
	Cut("jet3pT", "pT_jet3 > 40 GeV", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >= 40*GeV))),
	Cut("nbTag", "n btags == 0", lambda data: data.bJets.empty()),
	Cut("dPhi_jet_met", "dPhi_jet2_met >= 0.8", lambda data: (data.signalJets.size() >= 2) and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) >= 0.8),
	Cut("met", "met >= 200 GeV", lambda data: data.metRefFinal.met() >= 200*GeV),
	Cut("HT", "HTSig >= 12.5", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 12.5),
	Cut("mT", "mT in [60,90] GeV", lambda data: (data.eventInfo.auxdata(float)("mT") >= 60*GeV and data.eventInfo.auxdata(float)("mT") <= 90*GeV)),
	Cut("m_hadTop", "m_hadTop in [130,195] GeV", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 195)),
	Cut("aMT2", "aMT2 >= 120 GeV", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 120),
	])

###############
# tN_med TVR

add_virtual_cut_ex("tN_med_TVR", [
	Cut("nJets", "nJets >= 4 (25 GeV)", lambda data: n_jets_pt_larger(data, 4, 25*GeV)),
	Cut("jet1pT", "pT_jet1 > 80 GeV", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 80*GeV))),
	Cut("jet2pT", "pT_jet2 > 60 GeV", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >= 60*GeV))),
	Cut("jet3pT", "pT_jet3 > 40 GeV", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >= 40*GeV))),
	Cut("nbTag", "n btags >= 1", lambda data: data.bJets.size() >= 1),
	Cut("dPhi_jet_met", "dPhi_jet2_met >= 0.8", lambda data: (data.signalJets.size() >= 2) and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) >= 0.8),
	Cut("met", "met >= 200 GeV", lambda data: data.metRefFinal.met() >= 200*GeV),
	Cut("HT", "HTSig >= 12.5", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 12.5),
	Cut("mT", "mT in [90,120] GeV", lambda data: (data.eventInfo.auxdata(float)("mT") >= 90*GeV and data.eventInfo.auxdata(float)("mT") <= 120*GeV)),
	Cut("m_hadTop", "m_hadTop in [130,195] GeV", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 195)),
	Cut("aMT2", "aMT2 >= 120 GeV", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 120),
	])

###############
# tN_med WVR

add_virtual_cut_ex("tN_med_WVR", [
	Cut("nJets", "nJets >= 4 (25 GeV)", lambda data: n_jets_pt_larger(data, 4, 25*GeV)),
	Cut("jet1pT", "pT_jet1 > 80 GeV", lambda data: ((data.signalJets.size() >= 1) and (data.signalJets.at(0).pt() >= 80*GeV))),
	Cut("jet2pT", "pT_jet2 > 60 GeV", lambda data: ((data.signalJets.size() >= 2) and (data.signalJets.at(1).pt() >= 60*GeV))),
	Cut("jet3pT", "pT_jet3 > 40 GeV", lambda data: ((data.signalJets.size() >= 3) and (data.signalJets.at(2).pt() >= 40*GeV))),
	Cut("nbTag", "n btags == 0", lambda data: data.bJets.empty()),
	Cut("dPhi_jet_met", "dPhi_jet2_met >= 0.8", lambda data: (data.signalJets.size() >= 2) and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) >= 0.8),
	Cut("met", "met >= 200 GeV", lambda data: data.metRefFinal.met() >= 200*GeV),
	Cut("HT", "HTSig >= 12.5", lambda data: data.eventInfo.auxdata(float)("HTSig") >= 12.5),
	Cut("mT", "mT in [90,120] GeV", lambda data: (data.eventInfo.auxdata(float)("mT") >= 90*GeV and data.eventInfo.auxdata(float)("mT") <= 120*GeV)),
	Cut("m_hadTop", "m_hadTop in [130,195] GeV", lambda data: (data.eventInfo.auxdata(float)("TopMassChi2") >= 130 and data.eventInfo.auxdata(float)("TopMassChi2") <= 195)),
	Cut("aMT2", "aMT2 >= 120 GeV", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 120),
	])

