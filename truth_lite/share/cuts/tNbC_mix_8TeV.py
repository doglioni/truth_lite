
###############################################################################
# tNbC_mix

def cut_tNbC_mix_base(commonData, cutflow, weight):
       	#require at least 3 signal jets -- mimic former preselection cut!
	if commonData.signalJets.size() < 3:
		return 0
	cutflow.passCut(weight)

	# define pT cuts for 4 leading jets
	jet_pt_cuts = [80*GeV, 70*GeV, 50*GeV, 25*GeV]
	
	#require at least 4 signal jets
	if commonData.signalJets.size() < 4:
		return 0

	#check jet pT cuts
	for i, ptcut in enumerate(jet_pt_cuts):
		if commonData.signalJets.at(i).pt() < ptcut:
			return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal

        #dphi(met,jets)
	jet1 = commonData.signalJets.at(0)
	jet2 = commonData.signalJets.at(1)

	if _abs_delta_phi(jet1, met) < 0.6:
		return 0
	if _abs_delta_phi(jet2, met) < 0.6:
		return 0
	cutflow.passCut(weight)

	#dphi(met,lep)
	if _abs_delta_phi(_get_lep(commonData), met) < 0.6 :
		return 0
	cutflow.passCut(weight)

	#dR(l,jet1)
	if commonData.signalJets.at(0).p4().DeltaR(_get_lep(commonData).p4()) > 2.75:
		return 0
	cutflow.passCut(weight)

	#dR(l,bjet)
	if not commonData.bJets.empty() and commonData.bJets.at(0).p4().DeltaR(_get_lep(commonData).p4()) > 3.0:
		return 0
	cutflow.passCut(weight)

	#topness
	if commonData.eventInfo.auxdata(float)("Topness") < 2:
		return 0
	cutflow.passCut(weight)

	#mjjj
	if commonData.eventInfo.auxdata(float)("TopMassDist") > 360*GeV:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tNbC_mix_base", cut_tNbC_mix_base, [
       	"3 jets > 25",
      	"4 jets > 80,70,50,25",
	"DeltaPhi(j12,met) > 0.6",
	"DeltaPhi(l,met) > 0.6",
	"DeltaR(j1,l) < 2.75",
	"DeltaR(b1,l) < 3.0",
	"topness > 2",
	"mjjj < 360",
])


###############
# tNbC_mix SR

def cut_tNbC_mix_SR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tNbC_mix_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 270*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("mT") < 130*GeV :
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 190:
		return 0
	cutflow.passCut(weight)

	#MET/sqrt(HT)
	if commonData.eventInfo.auxdata(float)("METSig") < 9:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tNbC_mix_SR", cut_tNbC_mix_SR, [
	"tNbC_mix_base",
	">= 1 bjet",
	"MET > 270",
	"mT > 130",
	"amT2 > 190",
	"METSig > 9",
])

add_virtual_cut_ex("tNbC_mix_SR_new", [
	Cut("4j", "4 jets > 25", lambda data: data.eventInfo.auxdata(float)("presel_4j") > 0),
	Cut("j1_pt", "jet1 pT > 80", lambda data: data.signalJets.size() >= 1 and data.signalJets.at(0).pt >= 80*GeV),
	Cut("j2_pt", "jet2 pT >  70", lambda data: data.signalJets.size() >= 2 and data.signalJets.at(1).pt >=  70*GeV),
	Cut("j3_pt", "jet3 pT >  50", lambda data: data.signalJets.size() >= 3 and data.signalJets.at(2).pt >=  50*GeV),
	Cut("j4_pt", "jet4 pT >  25", lambda data: data.signalJets.size() >= 4 and data.signalJets.at(3).pt >=  25*GeV),
	Cut("1b", ">=1 bjet", lambda data: data.bJets.size() >= 1),
	Cut("bj_pt", "bjet1 pT > 60", lambda data: data.bJets.size() >= 1 and  data.bJets.at(0).pt >= 60*GeV),
	Cut("dphi_j1_met", "dPhi(j1,met) > 0.6", lambda data: data.signalJets.size() >= 1 and _abs_delta_phi(data.signalJets.at(0), data.metRefFinal) > 0.6),
	Cut("dphi_j2_met", "dPhi(j2,met) > 0.6", lambda data: data.signalJets.size() >= 2 and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) > 0.6),
	Cut("dphi_l_met", "dPhi(l,met) > 0.6", lambda data: _abs_delta_phi(_get_lep(data), data.metRefFinal) > 0.6),
	Cut("dR_j1_l", "dR(j1,l) < 2.75", lambda data: data.signalJets.size() >= 1 and data.signalJets.at(0).p4().DeltaR(_get_lep(data).p4()) < 2.75),
	Cut("dR_bjet_lep", "dR(lep,b-jet) <= 3", lambda data: data.bJets.size() >= 1 and data.bJets.at(0).p4().DeltaR(_get_lep(data).p4())<=3),
	Cut("met", "MET > 270", lambda data: data.metRefFinal.met() >= 270*GeV),
	Cut("metsig", "METSig > 9", lambda data: data.eventInfo.auxdata(float)("METSig") >= 9),
	Cut("mt", "mT > 130 GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 130*GeV),
	Cut("mjjj", "mjjj < 360", lambda data: data.eventInfo.auxdata(float)("TopMassDist") < 360*GeV),
	Cut("amt2", "amT2 >= 190", lambda data: data.eventInfo.auxdata(float)("aMT2_GeV") >= 190),
	Cut("topness", "topness > 2", lambda data: data.eventInfo.auxdata(float)("Topness") >= 2),
], save_sub_results=True)


###############
# tNbC_mix CRs

def cut_tNbC_mix_TCR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tNbC_mix_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 170*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 60*GeV or mT > 90*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 120:
		return 0
	cutflow.passCut(weight)

	#MET/sqrt(HT)
	if commonData.eventInfo.auxdata(float)("METSig") < 5:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tNbC_mix_TCR", cut_tNbC_mix_TCR, [
	"tNbC_mix_base",
	">= 1 bjet",
	"MET > 170",
	"mT in [60,90]",
	"amT2 > 120",
	"METSig > 5",
])


def cut_tNbC_mix_WCR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tNbC_mix_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 170*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 60*GeV or mT > 90*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 120:
		return 0
	cutflow.passCut(weight)

	#MET/sqrt(HT)
	if commonData.eventInfo.auxdata(float)("METSig") < 5:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tNbC_mix_WCR", cut_tNbC_mix_WCR, [
	"tNbC_mix_base",
	"== 0 bjet",
	"MET > 170",
	"mT in [60,90]",
	"amT2 > 120",
	"METSig > 5",
])




###############
# tNbC_mix VRs

def cut_tNbC_mix_TVR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tNbC_mix_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 170*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 90*GeV or mT > 120*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 120:
		return 0
	cutflow.passCut(weight)

	#MET/sqrt(HT)
	if commonData.eventInfo.auxdata(float)("METSig") < 5:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tNbC_mix_TVR", cut_tNbC_mix_TVR, [
	"tNbC_mix_base",
	">= 1 bjet",
	"MET > 170",
	"mT in [90,120]",
	"amT2 > 120",
	"METSig > 5",
])



def cut_tNbC_mix_WVR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("tNbC_mix_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal
	if met.met() < 170*GeV:
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 90*GeV or mT > 120*GeV:
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("aMT2_GeV") < 120:
		return 0
	cutflow.passCut(weight)

	#MET/sqrt(HT)
	if commonData.eventInfo.auxdata(float)("METSig") < 5:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("tNbC_mix_WVR", cut_tNbC_mix_WVR, [
	"tNbC_mix_base",
	"== 0 bjet",
	"MET > 170",
	"mT in [90,120]",
	"amT2 > 120",
	"METSig > 5",
])
