# some global constants

MeV = 1.0
GeV = 1e3 * MeV
TeV = 1e3 * GeV

# define a named tuple for the extended cuts (which are usable for N-1 plots)

## WARNING
# The following code, while being perfectly fine, brings ROOT's TPython
# interpreter to output a screen full of stupid error messages. The code
# works nontheless, but with this the first file loaded after _init.py
# is basically skipped.
# To keep ROOT from choking we replace the global Cut namedtuple with a
# function named Cut, that just deferes the call to the namedtuple 
# constructor. With this there is no global namedtuple object and ROOT is
# happy.
#from collections import namedtuple
#Cut = namedtuple("Cut", "id title cut_func")

Cut = None
def STUPID_ROOT_WORKAROUND():
	# create the namedtuple _inside_ this function and
	# let it never leak into the global namespace.
	from collections import namedtuple
	CutType = namedtuple("Cut", "id title cut_func")

	# Here we create a closure that uses the namedtuple. As
	# ROOT does not look into the closure's namespace using
	# a namedtuple in here is fine.
	def CutFunc(id, title, cut_func):
		return CutType(id, title, cut_func)

	global Cut
	Cut = CutFunc

STUPID_ROOT_WORKAROUND()

# python interface

VirtualCuts = {}
VirtualCutNames = [] # a second list, to keep the registration order
VirtualCuts_ex = {}
VirtualCutNames_ex = []

def add_virtual_cut(name, cut_func, cutflow):
	for entry in cutflow:
		if "~" in entry:
			print "Error: Invalid cutflow for cut '%s'" % name
			print "       Do not use the character '~"
			return

	VirtualCutNames.append(name)

	VirtualCuts[name] = {
		'name': name, 
		'func': cut_func, 
		'cutflow': cutflow,
	}

def add_virtual_cut_ex(name, cutList, save_sub_results=False):
	VirtualCutNames_ex.append(name)

	VirtualCuts_ex[name] = {
		'name': name, 
		'cuts': cutList,
		'save_sub_results': save_sub_results,
	}

# some standard functions which are needed often

def _abs_delta_phi(p1, p2):
	phi1 = p1.phi()
	phi2 = p2.phi()

	return abs(ROOT.Math.VectorUtil.Phi_mpi_pi(phi1 - phi2))

def _get_lep(cd):
	if cd.signalElectrons.empty():
		return cd.signalMuons.at(0)
	return cd.signalElectrons.at(0)

def n_jets_pt_larger(data, n, ptCut):
	if data.signalJets.size() < n:
		return False
	for i in xrange(n):
		if data.signalJets.at(i).pt() < ptCut:
			return False

	return True


# C++ interface

def VC_get_num_cuts():
	return len(VirtualCutNames)

def VC_get_cut_name(n):
	return VirtualCutNames[n]

def VC_get_cutflow_num_cuts(name):
	return len(VirtualCuts[name]['cutflow'])

def VC_get_cutflow_entry(name, n):
	return VirtualCuts[name]['cutflow'][n]

def VC_eval_cut(name):
	return VirtualCuts[name]['func'](gCommonData, gCurrentCutflow, gCommonData.weight)

# C++ interface for cuts with N-1 possibility
def VC_ex_get_num_cuts():
	return len(VirtualCutNames_ex)

def VC_ex_get_cut_name(n):
	return VirtualCutNames_ex[n]

def VC_ex_do_save_subcuts(name):
	return VirtualCuts_ex[name]['save_sub_results']

def VC_ex_get_num_sub_cuts(n):
	return len(VirtualCuts_ex[n]["cuts"])

def VC_ex_get_sub_cut_title(n, i):
	return VirtualCuts_ex[n]["cuts"][i].title

def VC_ex_get_sub_cut_id(n, i):
	return VirtualCuts_ex[n]["cuts"][i].id

def VC_ex_eval_sub_cut(n, i):
	return VirtualCuts_ex[n]["cuts"][i].cut_func(gCommonData)

