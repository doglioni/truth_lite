
###############################################################################
#bVeto

def cut_bVeto_base(commonData, cutflow, weight):
	# define pT cuts for 4 leading jets
	jet_pt_cuts = [80*GeV, 40*GeV, 30*GeV]

	#require at least 3 signal jets
	if commonData.signalJets.size() < 3:
		return 0

	#check jet pT cuts
	for i, ptcut in enumerate(jet_pt_cuts):
		if commonData.signalJets.at(i).pt() < ptcut:
			return 0
	cutflow.passCut(weight)

	met = commonData.metRefFinal

	if met.met() < 140*GeV:
		return 0
	cutflow.passCut(weight)

        #dphi(met,jets)
	jet1 = commonData.signalJets.at(0)
	jet2 = commonData.signalJets.at(1)

	if _abs_delta_phi(jet1, met) < 2.0:
		return 0
	cutflow.passCut(weight)

	if _abs_delta_phi(jet2, met) < 0.8:
		return 0
	cutflow.passCut(weight)

	#dR(l,jet1)
	if commonData.signalJets.at(0).p4().DeltaR(_get_lep(commonData).p4()) > 2.4 or commonData.signalJets.at(0).p4().DeltaR(_get_lep(commonData).p4()) < 0.8:
		return 0
	cutflow.passCut(weight)

	#MET/sqrt(HT)
	if commonData.eventInfo.auxdata(float)("METSig") < 5:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVeto_base", cut_bVeto_base, [
	"3 jets > 80,40,30",
       	"MET > 140",
	"DeltaPhi(j1,met) > 2.0",
	"DeltaPhi(j2,met) > 0.8",
	"DeltaR(j1,l) in [0.8,2.4]",
       	"METSig > 5",
])


###############
# bveto SR

def cut_bVeto_SR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("bVeto_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	if commonData.eventInfo.auxdata(float)("mT") < 120*GeV :
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVeto_SR", cut_bVeto_SR, [
	"bVeto_base",
	"== 0 bjet",
	"mT > 120",
])


add_virtual_cut_ex("bVeto_SR_old", [
	Cut("3j", "3 jets > 25", lambda data: data.eventInfo.auxdata(float)("presel_3j") > 0),
	Cut("j1_pt", "jet1 pT > 80", lambda data: data.signalJets.size() >= 1 and data.signalJets.at(0).pt >= 80*GeV),
	Cut("j2_pt", "jet2 pT >  40", lambda data: data.signalJets.size() >= 2 and data.signalJets.at(1).pt >=  40*GeV),
	Cut("j3_pt", "jet3 pT >  30", lambda data: data.signalJets.size() >= 3 and data.signalJets.at(2).pt >=  30*GeV),
	Cut("0b", "==0 bjet", lambda data: data.bJets.size() == 0),
	Cut("lep_eta","|lepton eta| <= 1.2", lambda data: abs(_get_lep(data).eta)<=1.2),
	Cut("dphi_j1_met", "dPhi(j1,met) > 2.0", lambda data: data.signalJets.size() >= 1 and _abs_delta_phi(data.signalJets.at(0), data.metRefFinal) > 2.0),
	Cut("dphi_j2_met", "dPhi(j2,met) > 0.8", lambda data: data.signalJets.size() >= 2 and _abs_delta_phi(data.signalJets.at(1), data.metRefFinal) > 0.8),
	Cut("dR_j1_l", "dR(j1,l) in [0.8, 2.4]", lambda data: data.signalJets.size() >= 1 and data.signalJets.at(0).p4().DeltaR(_get_lep(data).p4()) < 2.4 and  data.signalJets.at(0).p4().DeltaR(_get_lep(data).p4()) > 0.8),
	Cut("met", "MET > 140", lambda data: data.metRefFinal.met() >= 140*GeV),
	Cut("metsig", "METSig > 5", lambda data: data.eventInfo.auxdata(float)("METSig") >= 5),
	Cut("mt", "mT > 120 GeV", lambda data: data.eventInfo.auxdata(float)("mT") >= 120*GeV),
], save_sub_results=True)


###############
# bVeto_TCR 60-90/90-120

def cut_bVeto_TCR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("bVeto_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 60*GeV or mT > 90*GeV:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVeto_TCR", cut_bVeto_TCR, [
	"bVeto_base",
	">= 1 bjet",
       	"mT in [60,90]",
])


###############
# bVeto_WCR 60-90/90-120

def cut_bVeto_WCR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("bVeto_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 60*GeV or mT > 90*GeV:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVeto_WCR", cut_bVeto_WCR, [
	"bVeto_base",
	"== 0 bjet",
       	"mT in [60,90]",
])


###############
# bVeto_TVR 60-90/90-120

def cut_bVeto_TVR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("bVeto_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 90*GeV or mT > 120*GeV:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVeto_TVR", cut_bVeto_TVR, [
	"bVeto_base",
	">= 1 bjet",
       	"mT in [90,120]",
])

###############
# bVeto_WVR 60-90/90-120

def cut_bVeto_WVR(commonData, cutflow, weight):
	if commonData.eventInfo.auxdata(float)("bVeto_base") <= 0:
		return 0
	cutflow.passCut(weight)

	if not commonData.bJets.empty():
		return 0
	cutflow.passCut(weight)

	mT = commonData.eventInfo.auxdata(float)("mT")
	if mT < 90*GeV or mT > 120*GeV:
		return 0
	cutflow.passCut(weight)

	return weight

add_virtual_cut("bVeto_WVR", cut_bVeto_WVR, [
	"bVeto_base",
	"== 0 bjet",
       	"mT in [90,120]",
])
