#!/usr/bin/env python
import sys
import os

if '_ARGCOMPLETE' not in os.environ:
	_orig_argv = sys.argv[:]
	sys.argv = [_orig_argv[0]]

	import ROOT
	ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
	from ROOT import *

	sys.argv = _orig_argv

import truth_lite.SampleTools as Smp

def parse_options():
	import argparse
	#import truth_lite.ext.argcomplete as argcomplete
	import truth_lite.ArgTools as AT

	parser = argparse.ArgumentParser()
	parser.add_argument("sample", choices=AT.get_samples() + ['dummy'], help="The sample name")
	parser.add_argument("-m", "--meta-data", action="store_true", help="Print the meta data")
	parser.add_argument("-f", "--files", action="store_true", help="Print files")
	parser.add_argument("--full-path", help="Full path to sample is provided")

	
	#argcomplete.autocomplete(parser)
	opts = parser.parse_args()

	if not opts.full_path:
		opts.sample_path = Smp.input_path(opts.sample, validate=True)
	else:
		opts.sample_path = opts.full_path
 
	return opts

def main():
	opts = parse_options()

	sh = Smp.load(opts.sample_path)
	nsub = sh.size()

	print "Loaded SampleHandler with", nsub, "sub sample%s" % (nsub == 1 and "" or "s")
	print ""

	for i in xrange(nsub):
		smp = sh.at(i)
		nf = smp.numFiles()
		n_evt = smp.getMetaDouble("nc_nevt", -1)
		if n_evt == -1:
			n_evt = "unknown"
		else:
			n_evt = str(n_evt)

		print "%s (%d file%s, %s event%s)" % (smp.name(), nf, (nf == 1 and "" or "s"), n_evt, (n_evt == "1" and "" or "s"))

		if opts.files:
			smp.printContent()

		if opts.meta_data:
			meta = smp.meta()
			it = meta.MakeIterator()
			obj = it.Next()
			while obj:
				print "%s: %s" % (obj.GetName(), str(obj.value))
				obj = it.Next()

if __name__ == '__main__':
	main()
