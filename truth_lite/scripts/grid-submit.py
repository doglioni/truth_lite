#!/usr/bin/env python
import sys
import os

if '_ARGCOMPLETE' not in os.environ:
	_orig_argv = sys.argv[:]
	sys.argv = [_orig_argv[0]]

	import ROOT
	ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
	from ROOT import *

	sys.argv = _orig_argv
	
from truth_lite.RunTools import *

def load_grid_sample(opts):
	sh = SH.SampleHandler()
	SH.addGrid(sh, opts.dataset)
	sh.setMetaString("nc_tree", "CollectionTree")

	if opts.systematic:
		sh.setMetaString("systematic", opts.systematic)

	return sh

def main():
	opts = parse_options(Mode.grid)

	load_config(opts)
	sh = load_grid_sample(opts)
	job = create_job(opts, sh)

	grid_submit_job(opts, job)

if __name__ == '__main__':
	main()

