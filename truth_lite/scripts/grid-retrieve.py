#!/usr/bin/env python
import sys
import os
import pickle

if '_ARGCOMPLETE' not in os.environ:
	_orig_argv = sys.argv[:]
	sys.argv = [_orig_argv[0]]

	import ROOT
	ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
	from ROOT import *

	sys.argv = _orig_argv

import truth_lite.RunTools as RT
import truth_lite.SampleTools as Smp

def do_retrieve(opts):
	return EL.Driver.retrieve(opts.sample_path)

def resubmit_job(opts):
	driver_file = TFile.Open(os.path.join(opts.sample_path, "driver.root"))
	driver = driver_file.Get("driver")
	output_sample = driver.options().getString("nc_outputSampleName")
	driver_file.Close()

	sh = SH.SampleHandler()
	sh.load(os.path.join(opts.sample_path, "input"))

	opts_file = open(os.path.join(opts.sample_path, "swup-opts"), "r")
	submit_opts = pickle.load(opts_file)
	opts_file.close()

	RT.load_config(submit_opts)
	job = RT.create_job(submit_opts, sh)
	RT.grid_submit_job(submit_opts, job, fixed_output=output_sample)

def parse_options():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("--resubmit", action="store_true", help="Resubmit in case of an error")
	Smp.add_output_sample_opts(parser)

	opts = parser.parse_args()
	Smp.process_output_sample_opts(opts)

	return opts

def main():
	opts = parse_options()

	if do_retrieve(opts):
		# this is a HACK, data-ntuple may not always be present
		# TODO: fix this!
		if os.path.exists(os.path.join(opts.sample_path, "data-ntuple")):
			Smp.merge_result_trees(opts.sample_path, verbose=True)
		else:
			print "Guessing status: FAILED"
			if opts.resubmit:
				resubmit_job(opts)

if __name__ == '__main__':
	main()
