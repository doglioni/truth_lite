#!/usr/bin/env python
import sys
import os
import pipes

import truth_lite.GroupTools as Grp

def main():
	if len(sys.argv) < 2:
		print "Usage: submit-group.py [any arguments to submit.py...] <group-name>"
		return 1

	group = sys.argv[-1]
	submit_args = " ".join(pipes.quote(arg) for arg in sys.argv[1:-1])

	samples = Grp.get_group_samples(group)
	if len(samples) == 0:
		print "Group not found '%s'" % group
		return 1

	for smp in samples:
		cmd = "submit.py %s %s" % (submit_args, smp)
		print "Submit jobs for", smp
		os.system(cmd)


if __name__ == '__main__':
	main()
