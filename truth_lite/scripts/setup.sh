source truth_lite/share/truth_liteConfig.sh

ORIG_PATH=$PATH

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase 
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

#Need to setup python at SLAC
if [ "$CERN_USER" = "bnachman" ]; then
    localSetupPython 2.7.3-x86_64-slc6-gcc47
fi

if [ "$CERN_USER" = "" ]; then
	export CERN_USER=$USER
fi
export RUCIO_ACCOUNT=$CERN_USER

localSetupDQ2Client --quiet

if [ "$1" = "new" ]; then
	rcSetup $truth_lite_ANALYSIS_RELEASE
else
	rcSetup
fi


localSetupPandaClient --noAthenaCheck


# this is needed for Wuppertal's dCache system
export DCACHE_CLIENT_ACTIVE=1

export WorkDir=`pwd`
export PATH=$PATH:$WorkDir/truth_lite/scripts

export CALIBPATH=$WorkDir/truth_lite/share/:$CALIBPATH

if [ -n "$ZSH_VERSION" ]; then
   # assume Zsh
    echo "sorry, the argcomplete module doesn't fully work with python -> no tab-completion with zsh"
elif [ -n "$BASH_VERSION" ]; then
   # assume Bash
    source $WorkDir/truth_lite/scripts/_setup_tabcomplete.sh
else
   # asume something else
    echo "not sure about your SHELL"
fi

python $WorkDir/truth_lite/scripts/checkVersions.py

# reappend the old PATH, sometimes rcSetup messes this up
export PATH=$PATH:$ORIG_PATH

if [ -e local-setup.sh ]; then
  source local-setup.sh
fi
