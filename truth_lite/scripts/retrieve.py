#!/usr/bin/env python
import sys
import os
import glob

if '_ARGCOMPLETE' not in os.environ:
	_orig_argv = sys.argv[:]
	sys.argv = [_orig_argv[0]]

	import ROOT
	ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
	from ROOT import *

sys.argv = _orig_argv

import truth_lite.BatchTools
import truth_lite.SampleTools as Smp

def job_exists(job_id):
	return os.path.exists("../fetch/done-%s" % job_id)

def get_missing_jobs(opts):
	missing = []
	try:
		segments_file = open("segments", "r")
		for line in segments_file:
			job_id = line.split()[0]

			if not job_exists(job_id):
				missing.append(job_id)

		segments_file.close()
	except:
		print "ERROR, cannot open file 'segments', probably not a batch job."
		return None
	return missing

def resubmit(opts, run, job_id):
	if opts.skip_resubmission:
		return False
	truth_lite.BatchTools.submit("{run} {id}".format(run=run, id=job_id), opts.queue, opts)
	return True

def resubmit_missing(opts, missing):
	if opts.skip_resubmission or opts.only_failed:
		return
	print "Resubmitting missing/crashed jobs"
	for job_id in missing:
		resubmit(opts, opts.run_script, job_id)
	print "  ->", len(missing), "job%s submitted" % ("s" if len(missing) != 1 else "")

def resubmit_failed(opts, failed):
	if opts.skip_resubmission:
		return

	print "Resubmitting failed jobs"
	for entry in failed:
		job_id = entry[entry.rfind("-")+1:]
		if resubmit(opts, opts.run_script, job_id):
			os.remove(entry)
			os.remove("../fetch/done-" + job_id)
	print " -> ", len(failed), "job%s submitted" % ("s" if len(failed) != 1 else "")

def custom_merge_hists(opts, config):
	for sid in xrange(config.samples.size()):
		sample = config.samples[sid]

		out_path = os.path.join(opts.sample_path, "hist-" + sample.name + ".root")
		files_to_merge = []

		for seg in xrange(sample.begin_segments, sample.end_segments):
			segment = config.segments[seg]

			seg_file = os.path.join(opts.sample_path, "fetch", "hist-" + segment.name + ".root")

			is_done = os.path.exists(os.path.join(opts.sample_path, "fetch", "done-" + str(seg)))
			is_complete = os.path.exists(os.path.join(opts.sample_path, "fetch", "done-" + str(seg)))

			if is_done and is_complete and os.path.exists(seg_file):
				files_to_merge.append(seg_file)
			else:
				print "WARNING: Ignoring %s - not done" % (seg_file)

		hadd_cmd = "hadd %s %s" % (out_path, " ".join(files_to_merge))
		print hadd_cmd
		os.system(hadd_cmd)

def custom_retrieve(opts):
	print "Retrieving %s (custom)" % opts.sample_path

	configFile = TFile.Open(os.path.join(opts.sample_path, "submit", "config.root"))
	config = configFile.Get("job")

	custom_merge_hists(opts, config)

	EL.Driver.diskOutputSave(opts.sample_path, job)


def retrieve(opts):
	print "Retrieving %s" % opts.sample_path
	EL.Driver.retrieve(opts.sample_path)
	print "Trees are no longer automatically merged to ntuple.root, please use the export-sample.py tool"

def print_status(n_missing, n_failed):
	import truth_lite.ext.termcolor as tc
	n_bad = n_missing + n_failed
	color = 'green' if n_bad == 0 else 'red'
	msg = "Found %d bad job%s (%d failed, %d missing/crashed)" % (n_bad, n_bad==1 and "" or "s", n_failed, n_missing)
	tc.cprint(msg, color)

def parse_options():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-n", "--check-only", action="store_true", help="Only check for possible problems, but do not resubmit or retrieve. This option implies --skip-resubmission")
	parser.add_argument("-s", "--skip-resubmission", action="store_true", help="Do not resubmit failed jobs, corresponding samples are skipped.")
	parser.add_argument("-q", "--queue", default=truth_lite.BatchTools.default_queue(truth_lite.BatchTools.detect(silent=True)), help="The batch queue")
	parser.add_argument("-b", "--batch", help="Additional batch job options")
	parser.add_argument("-f", "--only-failed", help="Only resubmit failed jobs, do not care for missing jobs", action="store_true")
	parser.add_argument("--allow-failed", action="store_true")
	parser.add_argument("--directory", help="Ignore the sample, just retrieve this directory")
	Smp.add_output_sample_opts(parser)

	opts = parser.parse_args()


	if opts.directory:
		opts.sample_path = opts.directory

	if opts.check_only:
		opts.skip_resubmission = True

	return opts

def main():
	opts = parse_options()

	workDir = os.getenv("WorkDir")
	samples_to_retrieve = map(os.path.basename, glob.glob(os.path.join(workDir, "samples", opts.sample)))

	for smp in samples_to_retrieve:
		opts.sample = smp
		Smp.process_output_sample_opts(opts)

		print "Checking %s" % opts.sample_path
		if not os.path.isdir( "{submitDir}/submit".format(submitDir=opts.sample_path) ):
			print "this sample has no submit subdirecotry, probably was run locally. Will skip here."
			continue

		os.chdir("{submitDir}/submit".format(submitDir=opts.sample_path))
		opts.run_script = os.path.abspath("./run")

		missing = get_missing_jobs(opts)
		if missing is None:
			continue
		
		failed  = glob.glob("../fetch/fail-*".format(submitDir=opts.sample_path))

		num_bad = len(missing) + len(failed)
		print_status(len(missing), len(failed))

		if num_bad == 0 and not opts.check_only:
			retrieve(opts)
		elif num_bad > 0 and opts.allow_failed:
			custom_retrieve(opts)
		elif not opts.skip_resubmission:
			resubmit_missing(opts, missing)
			resubmit_failed(opts, failed)

if __name__ == '__main__':
	main()
