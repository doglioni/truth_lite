#!/usr/bin/env python
import os
import shlex
from subprocess import Popen, PIPE

def get_stdout(cmd):
    """
    Execute the external command and get its exitcode, stdout and stderr.
    """
    args = shlex.split(cmd)

    proc = Popen(args, stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    return out

def check_versions_number(output, versionsnumber, name):
	if output.find(versionsnumber) == -1:
		print "\033[1m\033[91mWarning:", name, "have been updated --> use \"source truth_lite/scripts/install.sh\"", '\033[0m'

################################

# check release version

cmd = "rc get_release"
out = get_stdout(cmd)

# the base is given as "Base,versionNumber"
base_version = os.environ['truth_lite_ANALYSIS_RELEASE'].split(",")[1]
check_versions_number(out, base_version, "AnalysisBase")




